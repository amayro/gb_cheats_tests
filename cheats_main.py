import datetime
import itertools
import json
import logging
import os
import pickle
import re
from time import sleep

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

try:
    from config import gb_login, gb_password
except:
    gb_login = ''
    gb_password = ''


class Geekbrains:
    """Класс для создания экземпляра скрипта"""

    def __init__(self):
        """Инициализация"""

        self.browser = webdriver.Chrome()
        self.url_gb = 'https://geekbrains.ru/'
        self.logger = self._get_logger()

        self._file_data_tests = 'data_tests.json'
        self._dir_tmp_screenshots = 'tmp_screenshots'
        self._exists_needed_files()

        self.current_test_id = None
        self._current_que_answ = None

    @staticmethod
    def _get_logger(show_logs=True):
        """Настройки логгера"""

        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        file_handler = logging.FileHandler('log_general.log')
        file_handler.setLevel(logging.INFO)

        logger_formatter = logging.Formatter(
            '[%(filename)s] [%(asctime)s] [LINE:%(lineno)03d] [%(levelname)s]: %(message)s')
        file_handler.setFormatter(logger_formatter)
        logger.addHandler(file_handler)

        if show_logs is True:
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.INFO)
            formatter = logging.Formatter('[%(asctime)s] [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
            console_handler.setFormatter(formatter)
            logger.addHandler(console_handler)

        return logger

    def _exists_needed_files(self):
        """
        Проверяет есть ли в директории необходимые для работы файлы и директории, если нету, тогда создает их

        **Param**:

        ``data_test``:
            база данных тестов
        ``tmp_screenshots``:
            папка для сохранения сделанных скриншотов
        """

        data_test = self._file_data_tests
        tmp_screenshots = self._dir_tmp_screenshots

        if not os.path.exists(data_test):
            with open(data_test, "w", encoding='utf-8') as file:
                file.write(json.dumps([], indent=4, ensure_ascii=False))

        if not os.path.exists(tmp_screenshots):
            os.makedirs(tmp_screenshots)

    def _web_click(self, selector: str):
        """Находит элемент и совершает клик по нему"""

        selector_el = self.browser.find_element_by_css_selector(selector)
        while True:
            try:
                selector_el = self.browser.find_element_by_css_selector(selector)
                selector_el.click()
                break
            except:
                sleep(1)
                self.browser.execute_script("arguments[0].scrollIntoView();", selector_el)

    def _web_wait(self, selector, timeout=3):
        """Ожидание подгрузки данных по селектору на странице"""

        WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))

    @staticmethod
    def _strip_str(string: str) -> str:
        """Очищает строку от чрезмерных символов \t, \n, \s в начале и конце строки"""

        return re.sub('^[\\n\\t\s]+|[\\n\\t\s]+$', '', string)

    def sign_in(self):
        """Логинится на сайте ГБ либо по паролю, либо с помощью cookie"""

        self.browser.get(self.url_gb)

        cookies_file = 'cookies.pkl'
        if not os.path.exists(cookies_file):
            self._web_wait('input[name="user[email]"]')
            field_login = self.browser.find_element_by_css_selector('input[name="user[email]')
            field_pass = self.browser.find_element_by_css_selector('input[name="user[password]"')

            field_login.send_keys(gb_login)
            field_pass.send_keys(gb_password)
            sleep(1)
            field_pass.send_keys(Keys.ENTER)

            with open(cookies_file, 'wb') as f:
                pickle.dump(self.browser.get_cookies(), f)

        else:
            with open(cookies_file, 'rb') as f:
                cookies = pickle.load(f)
                for cookie in cookies:
                    self.browser.add_cookie(cookie)
            self.browser.get(self.url_gb)

        sleep(5)

    def get_screenshot(self, prefix=''):
        """
        Получение скриншота со страницы

        **Args**:

        ``prefix``:
            Префикс названия файла, если необходимо
        """

        now_time = datetime.datetime.now()
        file_name = prefix + now_time.strftime("%d-%m-%Y_%H-%M-%S") + ".png"
        self.browser.get_screenshot_as_file(f'{self._dir_tmp_screenshots}/{file_name}')
        self.logger.info(f'Сделан новый скриншот: {file_name}')

    def solve_test(self, test_id: int = None):
        """
        Проверка на доступность и прохождение теста

        **Args**:

        ``test_id``:
            id теста
        """

        if test_id is None:
            test_id = self.current_test_id

        url = self.url_gb + 'tests/' + str(test_id)
        self.browser.get(url)
        self._web_wait('.test-control')

        soup = BeautifulSoup(self.browser.page_source, 'html.parser')
        title_test = soup.select_one('.header__title').text
        access_test = soup.select_one('.test-control__button')

        if not access_test:
            return self.logger.info(f'Тест пока не доступен: {title_test}')

        self.logger.info(f'Приступаю к прохождению теста {test_id}: {title_test}')
        self._web_click('.test-control__button')

        self._current_que_answ = {}
        data_test = self.load_data_test_json()
        if not data_test:
            data_test.update({
                'test_id': test_id,
                'title': title_test,
                'questions': []
            })

        result_banner_el = ''
        while not result_banner_el:
            sleep(1)
            data_test = self._answer_question(data_test)

            sleep(3)
            soup = BeautifulSoup(self.browser.page_source, 'html.parser')
            result_banner_el = soup.select_one('.gb-popup__close')

        self._web_click('.gb-popup__close')
        self.write_test_results(data_test)
        sleep(3)
        self.get_screenshot(prefix=f'T{test_id}_result_')

    def _answer_question(self, data_test: dict) -> dict:
        """
        Функция отвечает за ответ на вопрос на странице

        **Args**:

        ``data_test``:
            имеющиеся данные по тесту на момент ответа на вопрос

        :return: обновленные данные по тесту
        """

        self._web_wait('#answer-button')
        data_questions = data_test['questions']

        #  Обработка на случай долгого получения следующего вопроса
        #  (проверка на существование выбранного ответа или ошибки на стр)
        attempt = 0
        while attempt < 5:
            soup = BeautifulSoup(self.browser.page_source, 'html.parser')
            question_wrap = soup.select_one('#questions-wrapper')
            error_exist = question_wrap.select_one('.text-danger').text
            selected_exist = question_wrap.select_one('li[class="active"]')

            if error_exist:
                self._error_get_answers()
                return data_test

            if not selected_exist:
                break

            attempt += 1
            sleep(30)
        else:
            sleep(10 * 60)
            raise Exception('Сервер долго не выдал следующий вопрос. Переход к следующему тесту..')

        question_id = int(question_wrap.select_one('div[data-question-id]')['data-question-id'])
        qustion_text = self._strip_str(question_wrap.select_one('h3').text)
        answers_el = question_wrap.select('.list-unstyled > li')
        answers = [{'id': int(i.select_one('input')['data-id']), 'text': self._strip_str(i.text)} for i in answers_el]
        type_answers = question_wrap.select_one('input')['type']

        for i, d_que in enumerate(data_questions[:]):
            #  Если вопрос есть в базе, но не сходится число ответов, тогда:
            if question_id == d_que['id'] and len(answers) != len(d_que['answers']):
                self.logger.error(f"Несоответвие кол-ва ответов на вопрос id={d_que['id']}."
                                  f"В базе {len(d_que['answers'])}, а на странице {len(answers)}. "
                                  f"Вопрос: {d_que['text']}")

                #  Если в базе меньше вариантов ответов, чем на странице => сброс имеющихся данных по вопросу
                if len(answers) > len(d_que['answers']):
                    self.logger.error(f"Сбрасываю имеющиеся данные по вопросу..")
                    del data_questions[i]
                    continue

                #  Значит в базе больше число возможных ответов => если в базе есть ответ,
                #  помеченный как правильный, и он же есть на странице, тогда выбираем его
                elif d_que['right_answers'] and (d_que['right_answers'] in (answ['id'] for answ in answers)
                                                     if isinstance(d_que['right_answers'], int)
                                                     else all((r in (answ['id'] for answ in answers)
                                                               for r in d_que['right_answers']))):
                    self.logger.error(f"На странице есть ответ, помеченный как правильный. "
                                      f"Выбираю его.. [{d_que['right_answers']}]")
                    self.get_screenshot(prefix=f'T{self.current_test_id}_error_L_')

                #  перезагрузка страницы
                else:
                    self._error_get_answers()
                    return data_test

            #  Если вопрос есть в базе, а правильный ответ еще не найден
            if question_id == d_que['id'] and not d_que.get('right_answers'):
                if type_answers == 'radio':
                    for answer in answers:
                        if answer['id'] not in d_que['wrong_answers']:
                            select_answers = answer['id']
                            self._add_current_answer(question_id, d_que['answers'], select_answers)
                            data_questions[i]['wrong_answers'].append(select_answers)
                            break
                    else:
                        self._alert_full_wrong_answers(d_que['id'], data_questions[i]['wrong_answers'])
                        data_questions[i]['wrong_answers'] = []
                        return data_test

                elif type_answers == 'checkbox':
                    answers_id = [answer['id'] for answer in answers]
                    answers_id.sort()

                    variations_answers = []
                    for r in range(2, len(answers_id) + 1):
                        variations_answers.extend(list(itertools.combinations(answers_id, r)))

                    for variation in variations_answers:
                        if list(variation) not in d_que['wrong_answers']:
                            select_answers = variation
                            self._add_current_answer(question_id, d_que['answers'], select_answers)
                            data_questions[i]['wrong_answers'].append(select_answers)
                            break
                    else:
                        self._alert_full_wrong_answers(d_que['id'], data_questions[i]['wrong_answers'])
                        data_questions[i]['wrong_answers'] = []
                        return data_test
                break

            #  Если вопрос есть в базе и правильный ответ найден
            elif question_id == d_que['id']:
                select_answers = d_que['right_answers']
                self._add_current_answer(question_id, d_que['answers'], select_answers)
                break

        else:
            if type_answers == 'radio':
                select_answers = answers[0]['id']

            elif type_answers == 'checkbox':
                answers_id = [answer['id'] for answer in answers]
                answers_id.sort()

                variations_answer = list(itertools.combinations(answers_id, 2))
                select_answers = variations_answer[0]

            self._add_current_answer(question_id, answers, select_answers)

            data_questions.append({
                'id': question_id,
                'text': qustion_text,
                'answers': answers,
                'type_answers': type_answers,
                'right_answers': [],
                'wrong_answers': [select_answers]
            })

        if isinstance(select_answers, int):
            self._web_click(f'input[data-id="{select_answers}"]')
        else:
            for select in select_answers:
                sleep(0.5)
                self._web_click(f'input[data-id="{select}"]')

        sleep(1)
        self._web_click('#answer-button')
        return data_test

    def _add_current_answer(self,
                            question_id: int,
                            question_answers: list,
                            selected: int or list or tuple):
        """
        Добавляет в атрибут отвеченных вопросов для текущего теста
        id вопроса как key и текст ответа на него как value, при этом type(value) is list,
        для дальнейшей проверки при записи результатов,
        т.к. на странице результатов не выводятся id вопросов и ответов

        **Args**:

        ``question_id``:
            id вопроса, на который отвечаем

        ``question_answers``:
            доступные варианты ответов

        ``selected``:
            выбранные ответы
        """

        if isinstance(selected, int):
            self._current_que_answ[question_id] = [q['text'] for q in question_answers if q['id'] == selected]
        else:
            self._current_que_answ[question_id] = [q['text'] for q in question_answers if q['id'] in selected]

    def _alert_full_wrong_answers(self, que_id: int, wrong_answers: list):
        """Оповещает, если перебраны все комбинации, а ответ не найден

        **Args**:

        ``que_id``:
            id вопроса, на который отвечаем
        ``wrong_answers``:
            ошибочные ответы, имеющиеся на момент возникновения ошибки
        """

        self.logger.error(f"Перебраны все комбинации, а ответ не найден! "
                          f"question_id={que_id} "
                          f"combinations_used={wrong_answers}")
        self.get_screenshot(prefix=f'T{self.current_test_id}_error_F_')

    def _error_get_answers(self):
        """Обновляет страницу и оповещает, если произошла ошибка получения списка ответов для текущего вопроса
        или ошибка при отправке ответа на сервер"""

        self.logger.error(f"Ошибка получения вопроса. Обновление страницы..")
        name_pref = f'T{self.current_test_id}_error_G_'
        self.get_screenshot(prefix=name_pref)
        sleep(10)
        self.browser.refresh()
        sleep(10)
        self.get_screenshot(prefix=name_pref)

    def write_test_results(self, new_data):
        """Определяет правильные ответы и записывает все данные результата в файл с учетом того,
        что на странице результатов нет никакой информации о неправильных вопросах и их ответов
        кроме как 'сухого' текста"""

        self.logger.info('Записываю результаты теста')
        soup = BeautifulSoup(self.browser.page_source, 'html.parser')
        failed_questions_el = soup.select('.failed-questions__item')
        failed_questions = []

        result_test_el = soup.select('.result-data__answers span')
        result_test = " ".join([i.text for i in result_test_el])
        self.logger.info(f'Результаты: {result_test}')

        for failed_que in failed_questions_el:
            question_title = self._strip_str(failed_que.select_one('.failed-questions__item-title').text)
            question_answer = self._strip_str(failed_que.select_one('.failed-questions__item-user-answer').text)
            question_answer = question_answer.split('\n')[0]
            failed_questions.append((question_title, question_answer))

        data_questions = new_data['questions']
        for i, que in enumerate(data_questions[:]):
            # Если вопрос не был задействован при прохождении теста => проверка следующего
            if que['id'] not in self._current_que_answ.keys():
                continue

            for f_que, f_answ in failed_questions[:]:
                # Если найден неправильный ответ на вопрос, то прервать цикл, не помечая верный ответ (в else)
                if que['text'].startswith(f_que) and \
                        any([q.startswith(f_answ) for q in self._current_que_answ[que['id']]]):
                    # Перестраховка на случай, если в базу уже просочился неправильный ответ как правильный
                    if data_questions[i]['right_answers']:
                        self.logger.error(f"Ошибочный правильный ответ! "
                                          f"question_id={que['id']} "
                                          f"right_answers={que['right_answers']} "
                                          f"wrong_answers={que['wrong_answers']}")
                        data_questions[i]['wrong_answers'] = [data_questions[i]['right_answers']]
                        data_questions[i]['right_answers'] = []

                    failed_questions.remove((f_que, f_answ))
                    break
            else:
                if not data_questions[i]['right_answers']:
                    data_questions[i]['right_answers'] = data_questions[i]['wrong_answers'].pop()

        # Если не все неправильные результаты пройдут проверку на неверные,
        # тогда вывести оставшиеся нераспознанные неверные ответы
        if failed_questions:
            for f_que, f_answ in failed_questions:
                self.logger.error(f"Нераспознанный неверный ответ! Вопрос => [{f_que}] Ответ => [{f_answ}]")

        self.dump_data_json(new_data)

    def load_data_test_json(self, test_id: int = None) -> dict:
        """
        Загружает данные по тесту из файла json, если есть

        :return: данные по тесту в виде словаря или {}
         """

        if test_id is None:
            test_id = self.current_test_id

        with open(self._file_data_tests, 'r', encoding='utf-8') as file:
            data = json.load(file)

        for d in data:
            if test_id == d['test_id']:
                return d

        return {}

    def dump_data_json(self, new_data: dict):
        """Определяет есть ли данные по текущему тесту в файле, если да, то обновляет их, если нет, то записывает"""

        with open(self._file_data_tests, 'r', encoding='utf-8') as file:
            data_file = json.load(file)

        if data_file:
            for i, d in enumerate(data_file[:]):
                if new_data['test_id'] == d['test_id']:
                    del data_file[i]
                    break
            data_file.append(new_data)
        else:
            data_file = [new_data]

        with open(self._file_data_tests, "w", encoding='utf-8') as file:
            file.write(json.dumps(data_file, indent=2, ensure_ascii=False))

    def get_tests_id(self) -> list:
        """
        Парсинг всех тестов

        :return: список id открытых для прохождения тестов
        """
        url = self.url_gb + 'tests'
        self.browser.get(url)
        soup = BeautifulSoup(self.browser.page_source, 'html.parser')
        tests_id = re.findall('href=\"/tests/(\d+)\"', str(soup))
        return list(map(int, tests_id))

    def start(self, solve_test: int = None, first_test=1):
        """
        Запуск скрипта

        **Args**:

        ``solve_test``:
            id теста, который нужно пройти (если необходимо пройти только один тест)
        ``first_test``:
            с какого теста начать прохождение при полном обходе
        """

        self.logger.info(f'\n\n{"":-^100}\n{"Session started!":^100}\n{"":-^100}')
        self.sign_in()
        sleep(5)

        if solve_test:
            self.current_test_id = solve_test
            self.solve_test()
        else:
            tests = self.get_tests_id()[first_test - 1:]
            for i, test in enumerate(tests, start=1):
                self.logger.info(f'[{i}/{len(tests)}]')
                try:
                    self.current_test_id = test
                    self.solve_test()
                    sleep(5)
                except NoSuchElementException as error:
                    self.logger.error(error)
                except Exception as error:
                    self.logger.exception(error)

        self.logger.info(f'\n\n{"":-^100}\n{"Session END!":^100}\n{"":-^100}')


if __name__ == '__main__':
    if not gb_login or not gb_password:
        print('Данные от акканута не были введены. Выход.')
    else:
        Geekbrains().start(first_test=1)
        # Geekbrains().start(solve_test=166)
